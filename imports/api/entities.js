import { Mongo } from 'meteor/mongo';
import { FieldsSchema, DefinitionsSchema, LayoutsSchema, DocumentsSchema} from './schema';

// For Definitions
const Definitions = new Mongo.Collection('definitions');
Definitions.attachSchema(DefinitionsSchema);

//For Documents
const Documents = new Mongo.Collection('documents');
Documents.attachSchema(DocumentsSchema);

//For Fields
const Fields = new Mongo.Collection('fields');
Fields.attachSchema(FieldsSchema);

//For Layouts
const Layouts = new Mongo.Collection('layouts');
Layouts.attachSchema(LayoutsSchema);

export { Definitions, Documents, Fields, Layouts };
