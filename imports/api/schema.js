import SimpleSchema from 'simpl-schema';

// For Definitions
const FieldsSchema = new SimpleSchema({
    _id: {
        type: String,
        label: "Id"
    },
    label: {
        type: String,
        label: "Label"
    },
    name: {
        type: String,
        label: "Name"
    },
    type: {
        type: String,
        label: "Type"
    },
    maxLength: {
        type: Number,
        label: "Max Length",
        optional: true
    },
});

const FieldsCollectionSchema = new SimpleSchema({
    fields: [FieldsSchema]
});

const DefinitionsSchema = new SimpleSchema({
    layoutId: {
        type: String
    },
    schema: FieldsCollectionSchema
});

// For Layouts

const ColumnsSchema = new SimpleSchema({
    fieldId: {
        type: String
    }
});

const RowsSchema = new SimpleSchema({
    columns: [ColumnsSchema]
});


const HeaderSchema = new SimpleSchema({
    rows: [RowsSchema]
});

const LayoutsSchema = new SimpleSchema({
    buttons: [String],
    header: {
        type: HeaderSchema
    },
});

// For Documents
const FieldsValueSchema = new SimpleSchema({
    value: {
        type: String
    }
});

FieldsValueSchema.extend(FieldsSchema);
const DocumentsSchema = new SimpleSchema({
   fields: [FieldsValueSchema]
});

// export
export { DefinitionsSchema,  DocumentsSchema, FieldsSchema, LayoutsSchema }
