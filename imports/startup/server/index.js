import { Meteor } from "meteor/meteor";


import { Fields, Definitions, Layouts } from "../../api/entities";

Meteor.startup(() => {
    // initial data
    Fields.rawCollection().drop();
    Fields.insert({
        label: "Client Name",
        name: "name",
        type: "text",
        maxLength: 100
    });
    Fields.insert({
        label: "Client Age",
        name: "age",
        type: "number"
    });

    const fields = Fields.find().fetch();

    const columns = fields.map(function (field) {
        return {fieldId: field._id}
    });

    Layouts.rawCollection().drop();
    Layouts.insert({
        header: {
            rows: [
                { columns: columns }
            ]
        },
        buttons: ["Save new document"]
    }, function (err, _id) {
        Definitions.rawCollection().drop();
        Definitions.insert({
            schema: {
                fields: fields
            },
            layoutId: _id
        });
    });
});
