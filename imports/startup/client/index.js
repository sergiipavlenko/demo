import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

import '../../ui/components/navbar.html';
import '../../ui/body.html';
import '../../ui/components/footer.html';

import '../../ui/pages/home.html';
import '../../ui/pages/schema/schema.js';
import '../../ui/pages/documents/documents.js';


FlowRouter.route('/', {
    action() {
        BlazeLayout.render('layout', {home: 'Home', documentList: 'DocumentsList', schema: 'Schema'});
    },
});
